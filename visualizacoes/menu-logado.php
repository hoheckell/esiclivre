<?php
    if (empty($_SESSION[SISTEMA_CODIGO])) {
        exit;
    }
?>
<ul>
    <li class="opcao"><a href="../index.php">Início</a></li>
    <img src="../css/img/pipe.png" alt="Imagem E-sic Livre" />
    <li class="opcao"><a href="../solicitante">Alterar Cadastro</a></li>
    <img src="../css/img/pipe.png" alt="Imagem E-sic Livre"/>
    <li class="opcao"><a href="../alterasenha">Alterar Senha</a></li>
    <img src="../css/img/pipe.png" alt="Imagem E-sic Livre"/>
    <li class="opcao"><a href="../solicitacao">Fazer Solicitação</a></li>
    <img src="../css/img/pipe.png" alt="Imagem E-sic Livre"/>
    <li class="opcao"><a href="../acompanhamento">Solicitações Realizadas</a></li>
    <img src="../css/img/pipe.png" alt="Imagem E-sic Livre"/>
    <li class="opcao"><a href="../index/logout.php">Sair</a></li>
</ul>
