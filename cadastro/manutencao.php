<?php
/**********************************************************************************
 Sistema e-SIC Livre: sistema de acesso a informação baseado na lei de acesso.

 Copyright (C) 2014 Prefeitura Municipal do Natal

 Este programa é software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos da Licença GPL2.
***********************************************************************************/

	include_once("../inc/security.php");

	$erro   = "";	//grava o erro, se houver, e exibe por meio de alert (javascript) atraves da funcao getErro() chamada no arquivo do formulario. ps: a fun��o � declara em inc/security.php

        $acao 		= "";
        $nome 		= "";
        $cpfcnpj	= "";
        $profissao	= "";
        $idescolaridade	= "";
        $tipopessoa	= "";
        $idfaixaetaria	= "";
        $idtipotelefone	= "";
        $dddtelefone	= "";
        $telefone	= "";
        $email		= "";
        $confirmeemail	= "";
        $logradouro	= "";
        $cep		= "";
        $bairro		= "";
        $cidade		= "";
        $uf		= "";
        $numero		= "";
        $complemento	= "";
        $senha          = "";
        $confirmasenha	= "";


	//se tiver sido postado informação do formulario
	if( isset( $_POST['acao'] ) )
	{
		//dados solicitante
		$nome = filter_input( INPUT_POST, "nome" );
		$cpfcnpj = filter_input( INPUT_POST, "cpfcnpj" );
		$profissao = filter_input( INPUT_POST, "profissao" );
		$idescolaridade = filter_input( INPUT_POST, "idescolaridade" );
		$tipopessoa = filter_input( INPUT_POST, "tipopessoa" );
		$idfaixaetaria = filter_input( INPUT_POST, "idfaixaetaria" );
		$idtipotelefone = filter_input( INPUT_POST, "idtipotelefone" );
		$dddtelefone = filter_input( INPUT_POST, "dddtelefone" );
		$telefone = filter_input( INPUT_POST, "telefone" );
		$email = filter_input( INPUT_POST, "email" );
		$confirmeemail = filter_input( INPUT_POST, "confirmeemail" );

		//endereco
		$logradouro = filter_input( INPUT_POST, "logradouro" );
		$cep = filter_input( INPUT_POST, "cep" );
		$bairro = filter_input( INPUT_POST, "bairro" );
		$cidade = filter_input( INPUT_POST, "cidade" );
		$uf = filter_input( INPUT_POST, "uf" );
		$numero = filter_input( INPUT_POST, "numero" );
		$complemento = filter_input( INPUT_POST, "complemento" );

		//acesso e-sic
		$senha = filter_input( INPUT_POST, 'senha' );
		$confirmasenha = filter_input( INPUT_POST, 'confirmasenha' );

		include_once("../class/solicitante.class.php");

		$solicitante = new Solicitante();

		$solicitante->setNome($nome);
		$solicitante->setCpfCnpj($cpfcnpj);
		$solicitante->setProfissao($profissao);
		$solicitante->setIdEscolaridade($idescolaridade);
		$solicitante->setTipoPessoa($tipopessoa);
		$solicitante->setIdTipoTelefone($idtipotelefone);
		$solicitante->setIdFaixaEtaria($idfaixaetaria);
		$solicitante->setDDDTelefone($dddtelefone);
		$solicitante->setTelefone($telefone);
		$solicitante->setEmail($email);
		$solicitante->setLogradouro($logradouro);
		$solicitante->setCep($cep);
		$solicitante->setBairro($bairro);
		$solicitante->setCidade($cidade);
		$solicitante->setUf($uf);
		$solicitante->setNumero($numero);
		$solicitante->setComplemento($complemento);
		$solicitante->setConfirmeEmail($confirmeemail);
		$solicitante->setSenha($senha);
		$solicitante->setConfirmaSenha($confirmasenha);

		if (!$solicitante->cadastra()){
      $erro = $solicitante->getErro();
    }
		else
			echo "<script>alert('Cadastro realizado com sucesso!');location.href='index.php?r=$email';</script>";

		$solicitante = null;

	}

	if( empty($tipopessoa) ) $tipopessoa="F";
